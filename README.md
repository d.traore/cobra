# COBRA

Bonjour à toutes et tous.
Bonjour madame la ministre 
Bonjour chers delegué.


Je me nomme TRAORE Dipa, stagiare en prepa-informatique au CFRP-AVH

Je vais vous présenter quelque commande de base du GIT

git init : initialisation d'un projet (si le projet est créé depuis github, ne pas utiliser cette commande) ou réinitialisation d'un projet existant
git status : vérifier le statut, l'état (si modification de fichier) du répertoire de travail (le repository)
git add nom_du_fichier : ajoute un nouveau fichier (du répertoire de travail) à l'index
git add . : ajoute tous les fichiers à l'index
git commit -m "Phrase_explication_détaillée_du_commit" : Ajouter les fichiers de l'index dans un commit
git commit -am "Phrase_explication" : ajouter les fichiers au repositery, directement sans le add si le fichier est déjà ajouté - update de fichiers
git log : Voir l'historique des modifications du commit sous forme de liste
git checkout [sha_du_commit] : se positionner sur un ancien commit - attention le retour à un commit efface les commits les plus récents - si push après retour
git checkout master : retour au commit principal - le dernier
git clone [https_ssh_adresse_du_repositery_github] : rapatrier / clôner les sources d'un remote github vers un ordinateur local
git push origin master : envoyer les modifications des fichiers (le commit) vers github (le remote)
git pull origin master : récupérer les modifications effectuées par d'autres développeurs sur un remote
git pull [url_du_repositery] : récupérer les fichiers d'initialisation en local après la création d'un repositery distant
git branch [nom_de_la_branche] : créer une branche
git checkout [nom_de_la_branche_créée] : se positionner sur la branche créée (raccourci de création)
git checkout -b [nom_de_la_branche] : créer une branche et se positionner sur la branche créée
git push --set-upstream origin [branche-test-01] : envoyer la branche créée sur le remote Github
git push : envoyer les commit dans la branche créée sur le remote (possibilité d'ommettre origin et nom_de_la_branche)
git merge [nom_de_la_branche_à_fusionner] : fusionner les modifications de 2 branches, ici positionné sur master, la branche "nom_de_la_branche_à_fusionner" va fusionner avec la branche master. Attention, rien n'empêche de fusionner master avec une branche secondaire en se positionnant sur une branche secondaire et en codant -> git merge master. Merge ne supprime la branche
**git merge [nom_de_la_branche_à_fusionner] --no-ff ou - git merge --no-ff [nom_de_la_branche_à_fusionner] : Merger 2 branches - methode récursive - méthode conseillée pour forcer la création d'un commit et conserver une trace dans l'historique de développement
git branch -d [nom_de_la_branche_à_supprimer] : supprimer une branche en local
git push origin :[nom_de_la_branche_à_supprimer] : supprimer une branche sur le remote github (distante)
git blame [nom_du_fichier_à_vérifier] : savoir qui a modifié une ligne de code précise, la commande affiche une liste avec les modifications effectuées sur le fichier ainsi que le nom de la personne qui a fait la modification
git show [identifiant_du_sha] : afficher le détail précis des modifications sur un fichier et dans un commit précis. L'identifiant du sha est récupéré lors de l'utilisation de la commande blame. Cet identifiant est placé en début de ligne
git stash : mettre de côté des modifications sur un fichier en cours d'écriture (temporairement) - sans faire de commit - travailler sur d'autres modifications - permet alors de faire un commit des modifications sans faire de commit sur les modifications mises de côté
git stash pop : reprendre le développement en cours sur les modifications mises de côté - pop supprime les données dans stash
git remote set-url origin [new_url] : modifier l'url du repositery distant - origin ici (si le nom du repositery n'est pas modifié)
git commit --amend -m "nouveau_message" : modifier le dernier message de commit en local
git push [nom_du_remote nom_de_branche] --force ou -f : envoyer la modification du dernier message sur le repositery à distance
git remote add [nomcourt] [url] : Ajoute un dépôt distant au repositery local
git reset [nom_du_fichier] : Efface le fichier de l'index
git reset HEAD [nom_du_fichier] : Efface le fichier de l'index
git push --set-upstream origin master : permet d'utiliser la commande simple git push (sans origin master)
git rebase -i [numero_commit] : "naviguer" dans les commit - "retour en arrière" - permet de modifier la description d'un commit par exemple - le mode interactif (i pour interactive) ouvre l'editeur - attention ne jamais modifier un ancien commit public sur lequel travaille plusieurs personnes
git reset --hard HEAD^ : permet d'annuler un merge branch en local - si le push sur le repository n'est pas fait
git checkout -f: 'annuler' une suppression - si non commitée


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/d.traore/cobra.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/d.traore/cobra/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
